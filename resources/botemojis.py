# Various Emojis used by the bot.

checkmark = "<:greenTick:613702930444451880>"
x = "<:redTick:613703043283681290>"
member_leave = "<:member_leave:613363354357989376>"
member_join = "<:member_join:613361413272109076>"
mayushii = "<:mayushii:562686801043521575>"
meowawauu = "<:meowawauu:604760862049304608>"
kurisu = "<:kurisu:561618919937409055>"
python = "<:python:605592693267103744>"
dpy = "<:dpy:617883851162779648>"
postgres = "<:postgres:617886426318635015>"
# Presence
do_not_disturb = "<:dnd:572962188134842389>"
online = "<:online:572962188114001921>"
idle = "<:idle:572962188201820200>"
offline = "<:offline:572962188008882178>"
