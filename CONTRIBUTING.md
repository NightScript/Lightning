# Contributing to Lightning

First and foremost, thanks for taking the time to contribute! 👍

## I just have a question!

> Please don't make an issue just to ask a question. You'll get a faster answer using the resources below. 

There's an official Discord server and a site.
- [Lightning Hub Discord Server](https://discord.gg/cDPGuYd)
- [Lightning Site](https://lightning-bot.gitlab.io)

## I'm not a developer, but would like to help out!

There's a few ways you can help:
- Adding the bot to your server and spreading the word about Lightning
- Let us know your opinion on our [Support Server](https://discord.gg/cDPGuYd)!

## Setting up a development environment

You'll need the following for setting it up:

- Python 3.6/3.7+ (preferably Python3.7+)
- git - for cloning the repo. and adding commits
- A code editor (Visual Studio Code, Sublime Text, Atom, etc.)

Then, follow the instructions here -> [https://lightning-bot.gitlab.io/config/self-deploy/](https://lightning-bot.gitlab.io/config/self-deploy/)

## Submitting Changes

Send a [Merge Request to Lightning's Repository](https://gitlab.com/lightning-bot/Lightning/merge_requests) with a list of changes that have been made.
Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look something like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

    